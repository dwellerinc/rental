var webpack = require('webpack');
var path = require('path');
var mainDir = path.resolve(__dirname, 'src');
var version = require('./package.json').version;
var config = require('./webpack.production.config');

config.entry = [
  "webpack-dev-server/client?http://0.0.0.0:8080",
  'webpack/hot/only-dev-server',
  './app/scripts/router'
];
config.devtool = "eval";
config.debug = true;
config.plugins = [
  new webpack.HotModuleReplacementPlugin(),
  config.defineEnvPlugin
];

module.exports = config;
