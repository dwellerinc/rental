var webpack = require('webpack');
var path = require('path');
var mainDir = path.resolve(__dirname, 'src');
var version = require('./package.json').version;

var defineEnvPlugin = new webpack.DefinePlugin({
  'process.env': {
    NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
    VERSION: JSON.stringify(version)
  }
});

module.exports = {
  entry: {app: ['./app/scripts/router']},
  devtool: 'source-map',
  output: {
    path: path.join(__dirname, "public"),
    filename: 'bundle.js'
  },
  resolveLoader: {
    modulesDirectories: ['node_modules']
  },
  plugins: [
    new webpack.IgnorePlugin(/un~$/),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin(),
    defineEnvPlugin
  ],
  defineEnvPlugin: defineEnvPlugin,
  resolve: {
    extensions: ['', '.js', '.jsx', '.cjsx', '.coffee']
  },
  module: {
    preLoaders: [
      {test: /\.jsx?$/, loader: 'eslint', include: [mainDir]}
    ],
    loaders: [
      {test: /\.css$/, loader: 'style!css'},
      {test: /\.s[ac]ss/, loader: 'style!css!sass'},
      {test: /\.cjsx$/, loaders: ['react-hot', 'coffee', 'cjsx']},
      {test: /\.coffee$/, loader: 'coffee' },
      {test: /\.jsx?$/, loader: 'babel', query: {stage: 1}, include: [mainDir]}
    ]
  },
  externals: {
    react: 'React',
    immutable: 'Immutable'
  }
};
