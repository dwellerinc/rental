# @cjsx React.DOM

# TODO make this work.


describe 'HelloWorld', ->
  it 'should render an <h1> with the text "Hello World!"', ->
    React = require 'react/addons'
    TestUtils = React.addons.TestUtils

    HelloWorld = React.createClass
      displayName: 'HelloWorld'
      render: -> <div><h1>Hello world!</h1></div>

    # Render the HelloWorld component.
    helloWorld = TestUtils.renderIntoDocument(<HelloWorld />)

    # Verify the <h1>Hello World!</h1> element was created.
    h1 = TestUtils.findRenderedDOMComponentWithTag(helloWorld, 'h1')
    expect(h1.getDOMNode().textContent).toEqual "Hello world!"
