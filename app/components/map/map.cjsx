Link = require('react-router').Link
HouseStore = require('app/stores/house_store')
_ = require('underscore')

module.exports = React.createClass
  displayName: 'Map'
  gMap: null

  render: ->
    <div className='search-column'>
      <div id="map"></div>
    </div>

  getInitialState: ->
    gMap: null
    mapPins: []

  componentDidMount: ->
    @initMap()

  componentWillUnmount: ->

  componentWillReceiveProps: (newProps) ->
    @plotHouses(newProps.houses)

  initMap: ->
    if not @state.gMap
      mapOptions =
        center:
          lat: 37.759852,
          lng: -122.442915
        zoom: 12
      gMap = new (google.maps.Map)(document.getElementById('map'), mapOptions)
      @setState gMap: gMap

      google.maps.event.addListenerOnce gMap, 'tilesloaded', =>
        @plotHouses()
        google.maps.event.addListenerOnce gMap, 'tilesloaded', =>


  # _onChange: ->
  #   setTimeout @plotHouses, 1000

  plotHouses: (newHouses) ->
    #@state.map.setCenter(new (google.maps.LatLng)(37.759852, -122.442915))
    @removeMapMarkers()

    houses = newHouses ? @props.houses

    mapPins = []
    _.each houses, (house) =>
      latLng = new (google.maps.LatLng)(house.get('lat'), house.get('lng'))
      marker = new (google.maps.Marker)(
        position: latLng
        title: house.get('address')
        map: @state.gMap
      )
      mapPins.push(marker)

    @setState
      mapPins: mapPins

  removeMapMarkers: ->
    _.each @state.mapPins, (mapPin) ->
      mapPin.setMap(null)

    @setState
      mapPins: []
