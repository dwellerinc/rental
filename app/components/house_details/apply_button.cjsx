Link = require('react-router').Link
ClientActions = require 'app/actions/client_actions'
ApplicationStore = require 'app/stores/application_store'
Router = require 'react-router'

module.exports = React.createClass
  displayName: 'ApplyButton'
  mixins: [Router.Navigation]

  render: ->

    if @state.application
      text = 'Application Pending'
    else
      text = 'Apply for this house'

    <div>
      <button onClick={@onClick}>{text}</button>
    </div>


  getInitialState: ->
    @getStateFromStores()

  getStateFromStores: ->
    application: ApplicationStore.getOutgoingApplicationForHouse(@props.house)

  componentDidMount: ->
    ApplicationStore.addChangeListener(@onChange)

  componentWillUnmount: ->
    ApplicationStore.removeChangeListener(@onChange)

  onChange: ->
    @setState @getStateFromStores()

  onClick: ->
    if not @state.application
      @transitionTo("/application/#{@props.house.id}")