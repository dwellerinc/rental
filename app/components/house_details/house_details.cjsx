Link = require('react-router').Link
ClientActions = require 'app/actions/client_actions'
SearchSection = require 'app/components/search_form/search_section'
HouseStore = require 'app/stores/house_store'
ApplyButton = require 'app/components/house_details/apply_button'
UserStore = require 'app/stores/user_store'
FavoriteButton = require 'app/components/util/fav_button/fav_button'

imageWrapStyle =
  display: 'inline-block'
  position: 'relative'

imageStyle =
  height: 120
  margin: '0 5px 0 0'
  padding: 0


module.exports = React.createClass
  displayName: 'HouseDetails'

  render: ->
    house = @state.house

    apply = ''
    if house and (not house.get('user') or house.get('user').id != UserStore.getCurrentUser().id)
      apply = <ApplyButton house={house} />
      favButton = <FavoriteButton house={house} />

    if house
      images = house.get('images') || []
      <div>
        {apply} <br />
        {favButton} <br />
        Title: {house.get('title')} <br />
        Address: {house.get('Address')} <br />
        Price: ${house.get('price')} <br />
        Room Mates: {house.get('num_roommates')} <br />
        Bedrooms: {house.get('num_bedrooms')} <br />
        Start Date: {house.get('start_date')} <br />
        End Date: {house.get('end_date')} <br />

        <SearchSection section='People'
          categories={['lifestyle', 'personality', 'activities']}
          onValuesChanged={@_onValuesChanged }
          lifestyle={house.get('lifestyle')}
          personality={house.get('personality')}
          activities={house.get('activities')}
          interactive={false} />

        <SearchSection section='House'
          categories={['ammenities', 'type', 'living_space']}
          onValuesChanged={@_onValuesChanged}
          ammenities={house.get('ammenities')}
          type={house.get('type')}
          living_space={house.get('living_space')}
          interactive={false} />

        Images:
        <div>
          {images.map (image, key) =>
            <div key={key} style={imageWrapStyle}>
              <img src={image} style={imageStyle} />
          </div>}
        </div>

      </div>
    else
      <div>Loading</div>

  getInitialState: ->
    @getStateFromStores(@props.params.houseId)

  getStateFromStores: ->
    houseId = @props.params.houseId
    house = HouseStore.get(houseId)

    {
      houseId: houseId
      house: house
    }

  componentWillMount: ->


  componentDidMount: ->
    HouseStore.addChangeListener(@_onChange)
    house = HouseStore.get(@props.params.houseId)

    if not house
      ClientActions.fetchHouse(@state.houseId)
    else
      @setState
        house: house

  componentWillUnmount: ->
    HouseStore.removeChangeListener(@_onChange)

  _onChange: ->
    @setState @getStateFromStores()
