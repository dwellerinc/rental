Link = require('react-router').Link
ClientActions = require 'app/actions/client_actions'
DatePicker = require 'app/components/util/date/date_picker'
ApplicationStore = require 'app/stores/application_store'
HouseStore = require 'app/stores/house_store'
Router = require 'react-router'
References = require 'app/components/application_page/references'
_ = require 'underscore'

module.exports = React.createClass
  displayName: 'ApplicationPage'
  mixins: [Router.Navigation]

  render: ->
    <div>
      <h3>Applying for {@state.house.get('address') ? @state.house.get('Address')}</h3>
      <p>
        Select rent start date: <DatePicker onChange={@onValueChange.bind(@, 'startDate')} />
      </p>

      <p>
        Select minimum length of stay: &nbsp;
        <select value={@state.form.minLength} onChange={@onValueChange.bind(@, 'minLength')} >
          <option value="1">1 Month</option>
          <option value="2">2 Months</option>
          <option value="3">3 Months</option>
          <option value="4">4 Months</option>
          <option value="5">5 Months</option>
          <option value="6">6 Months</option>
          <option value="7">7 Months</option>
          <option value="8">8 Months</option>
          <option value="9">9 Months</option>
          <option value="10">10 Months</option>
          <option value="11">11 Months</option>
          <option value="12">12 Months</option>
        </select>
      </p>

      {
        @state.questions.map (question) =>
          <div>
            {question.text}: <br />
            <textarea value={@state.form.answers[question.id]}
              name="question-#{question.id}"
              onChange={@onAnswerChange.bind(@, question.id)}
            />
            <br />
          </div>
      }

      <References onChange={@onValueChange.bind(@, 'references')} />
      <br />

      <p>
        <button onClick={@submitApplication}>Submit Application</button>
      </p>
    </div>

  getInitialState: ->
    houseId = @props.params.houseId
    house = HouseStore.get(houseId)
    if house
      questions = house.get('questions') or []
      answers = []
      _.each questions, (question) ->
        answers[question.id] = ''

    form:
      startDate: null
      minLength: 1
      answers: answers
    house: house
    questions: questions

  componentDidMount: ->
    ApplicationStore.addChangeListener(@applicationSubmitted)

  componentWillUnmount: ->
    ApplicationStore.removeChangeListener(@applicationSubmitted)

  onValueChange: (key, val) ->
    form = @state.form

    if key == 'minLength'
      val = val.currentTarget.value

    form[key] = val

    @setState
      form: form

  onAnswerChange: (key, event) ->
    val = event.target.value
    form = @state.form
    form.answers[key] = val
    @setState
      form: form

  submitApplication: ->
    if not @state.form.startDate?
      alert('select start date')
      return

    ClientActions.submitApplication(@state.house, @state.form)

  applicationSubmitted: ->
    @transitionTo('/applications')