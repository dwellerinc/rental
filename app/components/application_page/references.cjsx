Link = require('react-router').Link
ClientActions = require 'app/actions/client_actions'
UserStore = require 'app/stores/user_store'

module.exports = React.createClass
  displayName: 'References'

  render: ->
    if not @state.editing
      <div>
        <button onClick={@startEditing} >Edit</button>
        {
          @state.references.map (reference, key) =>
            <div>
              Reference #{key+1} <br />
              Name: {reference.name} <br />
              Phone Number: {reference.phone} <br />
              Email Address: {reference.email} <br />
              <br />
            </div>
        }
      </div>

    else
      <div>
        {
          @state.references.map (reference, key) =>
            <div>
              Reference #{key+1} <br />
              Name:
              <input type='text'
                name="reference-name-#{key}"
                value={@state.references[key].name}
                onChange={@referenceChanged.bind(@, 'name', key)}
              />
              <br />

              Phone Number:
              <input type='text'
                name="reference-phone-#{key}"
                value={@state.references[key].phone}
                onChange={@referenceChanged.bind(@, 'phone', key)}
              />
              <br />

              Email Address:
              <input type='text'
                name="reference-email-#{key}"
                value={@state.references[key].email}
                onChange={@referenceChanged.bind(@, 'email', key)}
              />
              <br />
              <br />

            </div>
        }
        <br />
        <button onClick={@addReference} >Add Reference</button>
        <button onClick={@removeReference} >Remove Reference</button>

      </div>

  getInitialState: ->
    user = UserStore.getCurrentUser()
    references = user.get('references') or []

    if references.length > 0
      editing = false
    else
      editing = true
      references.push {name: '', email: '', phone: ''}

    user: user
    editing: editing
    references: references

  referenceChanged: (key, index, event) ->
    val = event.target.value

    references = @state.references
    if not references[index]
      references[index] = {}

    references[index][key] = val

    if @props.onChange
      @props.onChange(references)

    @setState
      references: references

  addReference: ->
    refs = @state.references
    refs.push {name: '', email: '', phone: ''}

    @setState
      references: refs

  removeReference: ->
    refs = @state.references
    if refs.length > 1
      refs.pop()

    @setState
      references: refs

  startEditing: ->
    @setState
      editing: true