Link = require('react-router').Link
AttributeRow = require('app/components/attribute_row')

module.exports = React.createClass
  displayName: 'PeopleForm'

  render: ->
    <div>
      <h3>{@props.section}</h3>
      {
        @props.categories.map (category) =>
          defaults = @props[category]
          <AttributeRow category={category} onChange={@_onValuesChanged.bind(@, category)}
            defaults={defaults}
            interactive={@props.interactive} />
      }
    </div>

  getInitialState: ->
    {}

  getDefaultProps: ->
    {interactive: true}

  _onValuesChanged: (key, values) ->
    if not @props.interactive
      return

    @setState
      key: values

    @props.onValuesChanged(key, values)