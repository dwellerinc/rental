Link = require('react-router').Link
LifestyleForm = require './lifestyle_form'
PersonalityForm = require './personality_form'
ActivityForm = require './activity_form'
AttributeRow = require('app/components/attribute_row')

module.exports = React.createClass
  displayName: 'PeopleForm'

  render: ->
    <div>
      <h3>People</h3>
      <AttributeRow category='lifestyle'/>
      <AttributeRow category='personality'/>
      <AttributeRow category="activities" />
    </div>

  getInitialState: ->
    null
