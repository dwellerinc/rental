

module.exports = React.createClass
  displayName: 'LivingSpaceForm'

  render: ->
    <div>
      Living Space
      <div>
        <input type="checkbox" name="private_bedroom" value="private_bedroom" /> Private Bedroom
      </div>
      <div>
        <input type="checkbox" name="shared_bedroom" value="shared_bedroom" /> Shared Bedroom
      </div>
      <div>
        <input type="checkbox" name="other_room" value="other_room" /> Other Room
      </div>
    </div>
