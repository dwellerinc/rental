AmmenitiesForm = require './ammenities_form'
HouseTypeForm = require './housing_type_form'
LivingSpaceForm = require './living_space_form'
AttributeRow = require('app/components/attribute_row')

module.exports = React.createClass
  displayName: 'HouseForm'

  render: ->
    <div>
      <h3>House Properties</h3>
      <AttributeRow category="ammenities" onChange={@_onValuesChanged.bind(@, 'ammenities')} />
      <AttributeRow category='type' onChange={@_onValuesChanged.bind(@, 'type')} />
      <AttributeRow category='living_space' onChange={@_onValuesChanged.bind(@, 'living_space')} />

    </div>

  getInitialState: ->
    null

  _onValuesChanged: (key, values) ->
    @setState
      key: values
