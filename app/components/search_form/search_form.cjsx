PeopleForm = require './people_form/people_form'
HouseForm = require './house_form/house_form'
LocationForm = require './location_form/location_form'
PriceSlider = require 'app/components/util/slider/price_slider'
SearchSection = require 'app/components/search_form/search_section'
clientActions = require 'app/actions/client_actions'
searchStore = require 'app/stores/search_store'
DatePicker = require 'app/components/util/date/date_range_picker'

module.exports = React.createClass
  displayName: 'PeopleForm'

  render: ->
    <div className="search-form">
      <PriceSlider onChange={@_onValuesChanged.bind(@, 'price')} />
      <DatePicker onChange={@_onValuesChanged.bind(@, 'dateAvailable')} />

      <SearchSection section='People' categories={['lifestyle', 'personality', 'activities']} onValuesChanged={@_onValuesChanged} />
      <SearchSection section='House' categories={['ammenities', 'type', 'living_space']} onValuesChanged={@_onValuesChanged} />

    </div>

  getInitialState: ->
    {}

  _onValuesChanged: (key, values) ->
    newState = @state
    if key == 'dateAvailable'
      # reversing start and end for dates because if you select 7-1 as a start date,
      # then you want the date available to be before 7-1 so the house is available on that date
      endDate = values.end
      values.end = values.start
      values.start = endDate

    newState[key] = values
    @setState newState

    clientActions.searchParamsChanged newState

# <SearchSection section='Location' categories={['neighborhood', 'nearby_services', 'environment']} />
