require './favorites.scss'

Link = require('react-router').Link
ClientActions = require 'app/actions/client_actions'
UserStore = require 'app/stores/user_store'
Map = require 'app/components/map/map'
HouseListings = require 'app/components/house_listings/house_listings'
HouseStore = require 'app/stores/house_store'

getStateFromStores = ->
  houses: HouseStore.getFavorites()

module.exports = React.createClass
  displayName: 'Favorites'

  render: ->

    <div>
      <h2>Favorites</h2>
      <div className='favorites-map'>
        <Map houses={@state.houses} />
      </div>
      <div className='favorites-listings'>
        <HouseListings houses={@state.houses} showPager={false} />
      </div>
    </div>

  getInitialState: ->
    getStateFromStores()

  componentDidMount: ->
    HouseStore.addChangeListener(@onChange)

  componentWillUnmount: ->
    HouseStore.removeChangeListener(@onChange)

  onChange: ->
    @setState getStateFromStores()