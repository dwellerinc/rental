require './image_input.scss'

_ = require 'underscore'

module.exports = React.createClass
  displayName: 'ImageInput'

  imageKeyCounter: 0

  render: ->
    <div className="image-input">
      <div>
        {@state.dataUris.map (image) =>
          <div key={image.key} className="image-card">
            <img src={image.uri} className="image-card-image" />
            <span onClick={=> @handleRemove image.key} className="image-card-close">X</span>
          </div>}
      </div>
      <label className="image-input-button">
        <input key={@state.fileInputVersion} type="file" multiple onChange={this.handleFile} style={{display:'none'}} />
        Add an Image
      </label>
    </div>

  getInitialState: ->
    fileInputVersion: 0
    dataUris: []

  componentWillReceiveProps: (newProps) ->
    if newProps.value?.length
      images = []
      _.each newProps.value, (image) =>
        images.push
          key: @imageKeyCounter++
          uri: image
      @setState
        dataUris: images

  onChange: ->
    @props.onChange(
      @props.name,
      @state.dataUris.map (image) -> image.uri
    )

  handleFile: (e) ->
    files = Array.prototype.slice.call e.target.files
    callbackCounter = files.length
    uris = @state.dataUris
    files.forEach (file) =>
      reader = new FileReader
      reader.onload = (upload) =>
        @setState(
          (state) =>
            fileInputVersion: @state.fileInputVersion + 1
            dataUris: @state.dataUris.concat [{
              key: @imageKeyCounter++
              uri: upload.target.result
            }]
          => @onChange() if --callbackCounter == 0
        )
      reader.readAsDataURL file

  handleRemove: (key) ->
    uris = @state.dataUris
    idx = -1
    for image in uris
      idx = uris.indexOf image if image.key == key
    left = uris.slice 0, idx
    right = uris.slice (idx + 1)
    @setState(
      (state) => dataUris: left.concat right
      @onChange
    )


