Link = require('react-router').Link
cx = require('classnames')

module.exports = React.createClass
  displayName: 'ToggleButton'

  render: ->
    classes = cx(@getSelectedClassName(), 'toggle-button')

    <div className={classes} onClick={@_onClick} >
      {this.props.text}
    </div>

  getDefaultProps: ->
    {
      interactive: true
      selected: false
    }

  getInitialState: ->
    {selected: @props.selected}

  getSelectedClassName: ->
    if @state.selected
      'button-selected'
    else
      'button-unselected'

  _onClick: ->
    if not @props.interactive
      return

    @setState
      selected: !@state.selected
    @props.onStateChange(!@state.selected)