Link = require('react-router').Link
ClientActions = require 'app/actions/client_actions'

module.exports = React.createClass
  displayName: 'DatePicker'

  render: ->
    <div>
      {@props.label}
      <input type='text' name='date' value={@state.dateText} ref='date'/>
    </div>

  getInitialState: ->
    date = @props.date

    date: date
    dateText: $.datepicker.formatDate("mm/dd/yy", date)

  dateNode: ->
    $(React.findDOMNode(@refs.date))

  componentDidMount: ->
    @dateNode().datepicker({
      changeMonth: true,
      changeYear: true
      numberOfMonths: 1
      maxDate: new Date()
      # minDate: '-100y'
      yearRange: '1910:2015'
      onSelect: (date, inst) =>
        @dateChanged(date)
      # onClose: (selectedDate) =>
      #   @birthdayNode().datepicker('option', 'maxDate', selectedDate)
    })

  componentWillUnmount: ->
    @dateNode().datepicker('destroy')

  dateChanged: (dateText) ->
    date = @dateNode().datepicker('getDate')

    @setState
      date: date
      dateText: dateText

    if @props.onChange
      @props.onChange(date)