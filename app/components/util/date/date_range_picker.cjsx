Link = require('react-router').Link

module.exports = React.createClass
  displayName: 'DatePicker'

  render: ->
    <div>
      <p>
        Date Available:
        <input type="text" id="start-datepicker" ref='start_datepicker' onChange={@onChange}/>
        <button onClick={@clearStartDate}>X</button>
      </p>

      <p>
        End Date (disabled):
        <input type="text" id="end-datepicker" ref='end_datepicker' onChange={@onChange}/>
        <button onClick={@clearEndDate}>X</button>
      </p>

    </div>

  getInitialState: ->
    {}

  componentDidMount: ->
    start_datepicker = @startNode()
    end_datepicker = @endNode()
    defaultStart = @props.start ? new Date()

    start_datepicker.datepicker
      defaultDate: defaultStart
      changeMonth: true
      numberOfMonths: 3
      minDate: new Date()
      onClose: (selectedDate) =>
        end_datepicker.datepicker 'option', 'minDate', selectedDate
      onSelect: (date, inst) =>
        @onChange date, inst

    end_datepicker.datepicker
      defaultDate: '+1w'
      changeMonth: true
      numberOfMonths: 3
      onClose: (selectedDate) =>
        start_datepicker.datepicker 'option', 'maxDate', selectedDate
      onSelect: (date, inst) =>
        @onChange date, inst

    # start_datepicker.datepicker()
    # end_datepicker.datepicker()

  componentWillUnmount: ->
    @startNode().datepicker('destroy')
    @endNode().datepicker('destroy')

  componentWillReceiveProps: (newProps) ->
    if newProps.start
      @startNode().datepicker('setDate', newProps.start)

  startNode: ->
    $(React.findDOMNode @refs.start_datepicker)

  endNode: ->
    $(React.findDOMNode @refs.end_datepicker)

  clearStartDate: ->
    @startNode().datepicker('setDate', null)
    @onChange()

  clearEndDate: ->
    @endNode().datepicker('setDate', null)
    @onChange()

  onChange: (a, b) ->
    startDate = @startNode().datepicker('getDate')
    # startDate = startDate ? Date.parse(startDate) ? null
    endDate = @endNode().datepicker('getDate')

    if @props.onChange
      @props.onChange({start: startDate, end: null})

