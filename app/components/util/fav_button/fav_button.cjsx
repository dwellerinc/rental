require './fav_button.scss'

Link = require('react-router').Link
ClientActions = require 'app/actions/client_actions'
HouseStore = require 'app/stores/house_store'

module.exports = React.createClass
  displayName: 'FavoriteButton'

  render: ->
    if @state.isFavorite
      image = 'images/red_heart.png'
    else
      image = 'images/white_heart.gif'

    <div>
      <img className='fav-heart' src={image} alt='heart' width='40' height='40' onClick={@onClick} onmouseover=''/>
    </div>

  getInitialState: ->
    state = @getStateFromStores()
    state.loading = false
    state

  getStateFromStores: ->
    isFavorite: HouseStore.isFavorite(@props.house)
    loading: false

  componentDidMount: ->
    HouseStore.addChangeListener(@onChange)

  componentWillUnmount: ->
    HouseStore.removeChangeListener(@onChange)

  onChange: ->
    @setState @getStateFromStores()

  onClick: (event)->
    # if @state.loading
    #   return

    ClientActions.toggleFavorite(@props.house, not @state.isFavorite)
    @setState
      loading: true

    event.stopPropagation()

