Link = require('react-router').Link

module.exports = React.createClass
  displayName: 'PriceSlider'

  render: ->
    <div>
      <p>
        <label htmlFor="search-price">Price range:</label>
        <input type="text" id="search-price" readOnly/>
      </p>

      <div id="slider-range" ref='slider'></div>
    </div>

  componentDidMount: ->

    $(React.findDOMNode(@refs.slider)).slider
      range: true
      min: 0
      max: 10000
      values: [
        0
        10000
      ]
      slide: (event, ui) ->
        $('#search-price').val '$' + ui.values[0] + ' - $' + ui.values[1]
      change: (event, ui) =>
        @onChange(event, ui)

    $('#search-price').val '$' + $('#slider-range').slider('values', 0) + ' - $' + $('#slider-range').slider('values', 1)

  onChange: (event, ui) ->
    min = parseInt(ui.values[0]) ? null
    max = parseInt(ui.values[1]) ? null

    if @props.onChange
      @props.onChange({start: min, end: max})
