require './house_listing.scss'

Link = require('react-router').Link
Router = require 'react-router'
HouseStore = require 'app/stores/house_store'
ClientActions = require 'app/actions/client_actions'
FavoriteButton = require 'app/components/util/fav_button/fav_button'

module.exports = React.createClass
  displayName: 'HouseListing'
  mixins: [Router.Navigation]
  clicked: false

  render: ->
    if @props.showFavoriteButton
      favButton = <div className='fav-button'><FavoriteButton house={@props.house} /></div>

    <div className="house_listing" onClick={@onClick.bind(@, @props.house.id)} >
      {favButton}
      <img src={@props.house.getMainImageUrl()} alt="house" width="200" height="100" />
      <div>
        {@props.house.get('title')}
      </div>
      <div>
        {@props.house.get('Address')}, {@props.house.get('City')}
      </div>
    </div>

  getInitialState: ->
    houseId: null

  componentDidMount: ->
    # HouseStore.addChangeListener(@_onChange)

  componentWillUnmount: ->
    # HouseStore.removeChangeListener(@_onChange)

  onClick: (houseId) ->
    @transitionTo("/house_details/#{houseId}")


