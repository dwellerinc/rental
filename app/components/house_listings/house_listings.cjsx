Link = require('react-router').Link
HouseStore = require('app/stores/house_store')
HouseListing = require('app/components/house_listings/house_listing')
Pager = require('app/components/house_listings/pager')
SearchStore = require('app/stores/search_store')
ClientActions = require 'app/actions/client_actions'

module.exports = React.createClass
  displayName: 'HouseListings'

  render: ->
    if @props.showPager
      pager = <div className="house-listings-pager">
                <Pager />
              </div>

    <div>
      {
        if @props.houses
          @props.houses.map (result) =>
            return <HouseListing house={result} key={result.id} showFavoriteButton={true}/>
      }

      {pager}
    </div>

  getInitialState: ->
    {}

  getDefaultProps: ->
    houses: []
    showPager: true

  componentDidMount: ->

  componentWillUnmount: ->

  componentWillReceiveProps: ->




