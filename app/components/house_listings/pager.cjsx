Link = require('react-router').Link
HouseStore = require('app/stores/house_store')
_ = require('underscore')
ClientActions = require('app/actions/client_actions')
SearchStore = require('app/stores/search_store')

getStateFromStores = ->
  {
    totalPages: HouseStore.getNumPages()
    pageSize: HouseStore.getPageSize()
    totalHouses: HouseStore.getAll().length
    currentPage: SearchStore.getCurrentPage()
  }

module.exports = React.createClass
  displayName: 'Pager'

  render: ->
    pages = _.range(@state.totalPages)
    startListing = (@state.currentPage * @state.pageSize) + 1
    endListing = Math.min(startListing + @state.pageSize, @state.totalHouses)
    <div>
      {startListing} - {endListing} of {@state.totalHouses} houses <br />
      {
        pages.map (page) =>
          <button onClick={@_changePage.bind(@, page)}>
            {page+1}
          </button>
      }
    </div>

  getInitialState: ->
    getStateFromStores()

  componentDidMount: ->
    HouseStore.addChangeListener(@_onChange)
    SearchStore.addChangeListener(@_onChange)

  componentWillUnmount: ->
    HouseStore.removeChangeListener(@_onChange)
    SearchStore.removeChangeListener(@_onChange)

  _onChange: ->
    @setState getStateFromStores()

  _changePage: (page) ->
    ClientActions.searchPageChanged(page)