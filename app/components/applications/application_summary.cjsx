Link = require('react-router').Link
ClientActions = require 'app/actions/client_actions'
HouseStore = require 'app/stores/house_store'

module.exports = React.createClass
  displayName: 'ApplicationSummary'

  render: ->
    if @state.house
      houseDetails = <div>{@state.house.get('address') ? @state.house.get('Address')}</div>
    else
      houseDetails = ''

    <div>
      {houseDetails}
      {@props.application.createdAt.toString()} <br />
      {@props.application.get('startDate').toString()} <br />
      {@props.application.get('minLength')} months <br />
    </div>

  getInitialState: ->
    @getStateFromStores()

  getStateFromStores: ->
    house = HouseStore.get(@props.application.get('house').id)

    house: house

  componentDidMount: ->
    HouseStore.addChangeListener(@onChange)

  componentWillUnmount: ->
    HouseStore.removeChangeListener(@onChange)

  onChange: ->
    @setState @getStateFromStores()