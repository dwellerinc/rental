Link = require('react-router').Link
ApplicationStore = require 'app/stores/application_store'
ApplicationSummary = require 'app/components/applications/application_summary'
_ = require 'underscore'
HouseStore = require 'app/stores/house_store'

module.exports = React.createClass
  displayName: 'Applications'

  render: ->
    <div>

      <h3>My Applications</h3>
      {
        @state.outgoingApplications.map (application) =>
          return <div><ApplicationSummary application={application} key={application.id} /><br /></div>
      }

      <h3>My Listings</h3>
      {
        _.map @state.incomingApplications, (apps, houseId) =>
          house = HouseStore.get(houseId)
          if house
            return <div>
              <h4>{house.get('address') ? house.get('Address')}</h4>
              {
                _.map apps, (application) ->
                  return <div><ApplicationSummary application={application} key={application.id} /><br /></div>
              }
            </div>
      }

    </div>


  getInitialState: ->
    @getStateFromStores()

  getStateFromStores: ->
    outgoingApplications: ApplicationStore.getOutgoingApplications()
    incomingApplications: ApplicationStore.getIncomingApplicationsByHouseId()

  componentDidMount: ->
    ApplicationStore.addChangeListener(@onChange)
    HouseStore.addChangeListener(@onChange)

  componentWillUnmount: ->
    ApplicationStore.removeChangeListener(@onChange)
    HouseStore.addChangeListener(@onChange)

  onChange: ->
    @setState @getStateFromStores()
