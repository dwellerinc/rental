User = require 'app/models/user'
House = require 'app/models/house'
Application = require 'app/models/application'

Link = require('react-router').Link
RouteHandler = require('react-router').RouteHandler
p = require('../api/parse')
NavBar = require 'app/components/nav_bar/nav_bar'
ClientActions = require 'app/actions/client_actions'
UserStore = require 'app/stores/user_store'
UrlStore = require 'app/stores/url_store'
Router = require 'react-router'
HouseStore = require 'app/stores/house_store'

module.exports = React.createClass
  displayName: 'HelloWorld'
  mixins: [Router.Navigation]

  render: ->
    <div>
      <NavBar />
      <RouteHandler />
    </div>

  componentWillMount: ->
    p.init()

  componentDidMount: ->
    # p.searchHouses()
    # p.fetchApplications()
    # ClientActions.appWillLaunch()

    # UrlStore.addChangeListener(@_urlChange)

  componentWillUnmount: ->
    # UrlStore.removeChangeListener(@_urlChange)

  _urlChange: ->
    @transitionTo(UrlStore.getNewUrl())

