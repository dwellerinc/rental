Link = require('react-router').Link
ClientActions = require 'app/actions/client_actions'
Router = require 'react-router'
UserStore = require 'app/stores/user_store'

module.exports = React.createClass
  displayName: 'Register'
  mixins: [Router.Navigation]

  render: ->
    <div>
      <p>
        First Name:
        <input type='text' name='firstName' value={@state.first_name} onChange={@valueChanged} />
      </p>

      <p>
        Last Name:
        <input type='text' name='lastName' value={@state.last_name} onChange={@valueChanged} />
      </p>

      <p>
        Age:
        <input type='text' name='age' value={@state.age} onChange={@valueChanged} />
      </p>

      <p>
        Email:
        <input type='text' name='email' value={@state.email} onChange={@valueChanged} />
      </p>

      <p>
        Date of Birth:
        <input type='text' name='dob' value={@state.dob} onChange={@valueChanged} />
      </p>

      <p>
        Password:
        <input type='password' name='password' value={@state.password} onChange={@valueChanged} />
      </p>

      <button onClick={@register}>Register</button>
    </div>

  getInitialState: ->
    form: {
      firstName: null
      lastName: null
      age: null
      email: null
      dob: null
    }

  componentDidMount: ->
    # UserStore.addChangeListener(@userChanged)

  componentWillUnmount: ->
    # UserStore.removeChangeListener(@userChanged)

  # userChanged: ->
  #   @transitionTo('/')

  valueChanged: (event) ->
    form = @state.form
    form[event.target.name] = event.target.value
    @setState
      form: form

  register: ->
    form = @state.form

    unless form.password and form.email
      window.alert('password and email required')
      return

    ClientActions.registerUser(form)
