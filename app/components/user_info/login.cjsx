Link = require('react-router').Link
ClientActions = require 'app/actions/client_actions'

module.exports = React.createClass
  displayName: 'PeopleForm'

  render: ->
    <div>
      <p>
        Email:
        <input type='text' name='username' value={@state.username} onChange={@valueChanged} />
      </p>
      <p>
        Password:
        <input type='password' name='password' value={@state.password} onChange={@valueChanged} />
      </p>
      <button onClick={@login}>Login</button>
      <br /> <br />
      <button onClick={@facebookLogin}>Facebook Login</button>
    </div>

  getInitialState: ->
    {
      email: null
      password: null
    }

  valueChanged: (event) ->
    name = event.target.name
    value = event.target.value
    state = @state
    state[name] = value
    @setState state

  login: ->
    ClientActions.login(@state.username, @state.password)

  facebookLogin: ->
    ClientActions.facebookLogin()

