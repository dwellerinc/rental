Link = require('react-router').Link
Parse = require 'app/api/parse'
ClientActions = require 'app/actions/client_actions'
UserStore = require 'app/stores/user_store'
DatePicker = require 'app/components/util/date/date_picker'

module.exports = React.createClass
  displayName: 'UserSettingsForm'

  render: ->
    if @state.confirmation
      confirmation = <p>Settings Updated</p>
    else
      confirmation = <div></div>

    if @state.fbLinked
      facebook = <a href={@state.user.get('facebookUrl')} target='_blank'>
                    Facebook Account Linked
                  </a>
    else
      facebook = <button onClick={@linkFacebookAccount}>
                    Link Facebook Account
                  </button>

    <div>
      {confirmation}
      <p>
        First Name:
        <input type='text' name='firstName' value={@state.form.firstName} onChange={@onChange} />
      </p>
      <p>
        Last Name:
        <input type='text' name='lastName' value={@state.form.lastName} onChange={@onChange} />
      </p>
      <p>
        <DatePicker onChange={@birthdayChanged} date={@state.form.birthday} label={'Birthday:'}/>
      </p>
      <p>
        Driver License Number <br /> (for background check):
        <input type='text' name='dlNumber' value={@state.form.dlNumber} onChange={@onChange} />
      </p>

      {facebook} <br /> <br />

      <button onClick={@saveForm}>Update</button>
    </div>

  getInitialState: ->
    user = UserStore.getCurrentUser()
    birthday = user.get('birthday')
    fbLinked = user.get('facebookId')?

    user: user
    form:
      firstName: user.get('firstName')
      lastName: user.get('lastName')
      dlNumber: user.get('dlNumber')
      birthday: birthday
    fbLinked: fbLinked

  componentDidMount: ->
    UserStore.addChangeListener(@_userChanged)

  componentWillUnmount: ->
    UserStore.removeChangeListener(@_userChanged)

  birthdayChanged: (date) ->
    form = @state.form
    form.birthday = date
    @setState
      form: form
      confirmation: false

  onChange: (event) ->
    form = @state.form
    form[event.target.name] = event.target.value
    @setState
      form: form
      confirmation: false

  saveForm: ->
    ClientActions.updateCurrentUser(@state.form)

  _userChanged: ->
    user = UserStore.getCurrentUser()

    @setState
      user: user
      confirmation: true
      fbLinked: user.get('facebookId')?

  linkFacebookAccount: ->
    ClientActions.linkFacebookAccount()
