Link = require('react-router').Link
UserStore = require('app/stores/user_store')
Router = require 'react-router'
ClientActions = require 'app/actions/client_actions'

imgStyle = {
  margin: 0
}

module.exports = React.createClass
  displayName: 'NavBar'
  mixins: [Router.Navigation]

  render: ->

    if @state.currentUser
      wholeName = @state.currentUser.get('firstName') + ' ' + @state.currentUser.get('lastName')
      imageSrc = @state.currentUser.get('fbPicture')

      extra = [
        <li><a href='#user_info'>
          <img src={imageSrc} style={imgStyle} />
          {wholeName}
        </a></li>,
        <li><a onClick={@logout}>Logout</a></li>
      ]
    else
      extra = [
        <li><a href='#login'>Login</a></li>,
        <li><a href='#register'>Register</a></li>
      ]

    <div>
      <nav className='nav_bar'>
        <li><a href="#">Home</a></li>
        <li><a href='#favorites'>Favorites</a></li>
        <li><a href="#post">Create Listing</a></li>
        <li><a href='#applications'>Applications</a></li>
        <li><a href='#my_listings'>My Listings</a></li>
        <li><a href='#messages'>Messages</a></li>
        {extra}
      </nav>
    </div>

  getInitialState: ->
    @getStateFromStores()

  getStateFromStores: ->
    currentUser: UserStore.getCurrentUser()

  componentDidMount: ->
    UserStore.addChangeListener(@_userChanged)

  componentWillUnmount: ->
    UserStore.removeChangeListener(@_userChanged)

  _userChanged: ->
    newUser = UserStore.getCurrentUser()
    @setState
      currentUser: newUser

    # ClientActions.navigateToUrl('/')
    # @transitionTo('/')

  logout: ->
    ClientActions.logout()



