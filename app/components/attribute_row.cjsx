Link = require('react-router').Link
ToggleButton = require('app/components/util/button/toggle_button')
attributes = require('app/constants/attributes')
_ = require('underscore')

module.exports = React.createClass
  displayName: 'AttributeRow'

  render: ->
    <div className='attr-row'>
      <div className='attr-row-category'>
        {@state.categoryName}
      </div>
      <div>
        {
          @state.attrs.map (attr) =>
            if attr.key in @props.defaults
              selected = true
            else selected = false
            <div className='attr-row-attribute'>
              <ToggleButton text={attr.value} key={attr.key} onStateChange={@_onToggle.bind(@, attr.key)}
                interactive={@props.interactive}
                selected={selected} />
            </div>
        }
      </div>
    </div>

  getDefaultProps: ->
    {
      defaults: []
      interactive: true
    }

  getInitialState: ->
    state = {
      attrs: attributes[@props.category].values
      categoryName: attributes[@props.category].pretty
      values: @props.defaults
    }
    return state

  _onToggle: (key, state) ->
    values = @state.values
    if not state
      values = _.without values, key
    else
      values.push key
    @setState
      values: values

    if @props.onChange
      @props.onChange(values)
