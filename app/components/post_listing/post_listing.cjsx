Link = require('react-router').Link
SearchSection = require 'app/components/search_form/search_section'
ImageInput = require 'app/components/util/image_input'
FileUpload = require 'app/components/util/file_uploader'
DatePicker = require 'app/components/util/date/date_range_picker'
UserStore = require 'app/stores/user_store'
ClientActions = require 'app/actions/client_actions'
_ = require 'underscore'
HouseStore = require 'app/stores/house_store'

module.exports = React.createClass
  displayName: 'PostListing'

  render: ->
    #<SearchSection section='People' categories={['lifestyle', 'personality', 'activities']} onValuesChanged={@_onValuesChanged} />

    <div>
      <div>
        Listing Title:
        <input type='text' name='title' value={@state.form.title} onChange={@valueChanged} />
      </div>
      <div>
        Address:
        <input type='text' name='address' autocomplete='false' value={@state.form.address} onChange={@valueChanged} ref='address' />
        <input type='text' name='city' value={@state.form.city} readOnly ref='city' />
        <input type='text' name='state' value={@state.form.state} readOnly ref='state' />
      </div>
      <div>
        Price: $
        <input type='text' name='price' value={@state.form.price} onChange={@valueChanged} />
      </div>
      <div>
        Number of Roommates:
        <input type='text' name='num_roommates' value={@state.form.num_roommates} onChange={@valueChanged} />
      </div>
      <div>
        Number of Bedrooms:
        <input type='text' name='num_bedrooms' value={@state.form.num_bedrooms} onChange={@valueChanged} />
      </div>

      <DatePicker onChange={@dateChanged} start={@state.form.dateAvailable} />

      <div>
        References Required?:
        <input type='checkbox' name='references_required' checked={@state.form.references_required} onChange={@valueChanged} />
      </div>

      <div>
        Online Payment Required?:
        <input type='checkbox' name='online_payment_required' checked={@state.form.online_payment_required} onChange={@valueChanged} />
      </div>
      <br />

      {
        _.map Array.apply(null, {length: @state.custom_question_count}).map(Number.call, Number), (index) =>
          return <div>
            Custom Question {index+1}:
            <input type='text' value={@state.form.questions[index]} name="question-#{index}" onChange={@questionChanged.bind(@, index)} />
          </div>
      }
      <button onClick={@addCustomQuestion} >Add Custom Question</button>
      <button onClick={@removeCustomQuestion}>Remove Custom Question</button>
      <br />

      <SearchSection section='House' categories={['ammenities', 'type', 'living_space']} onValuesChanged={@_onValuesChanged} />

      <div>
        Additional Info:
        <textarea value={@state.addInfo} name='add_info' onChange={@valueChanged} />
      </div>

      <div>
        Images:
        <ImageInput name='images' value={@state.form.images} onChange={@_onValuesChanged} />
      </div>

      <button onClick={@submit}>Submit</button>
    </div>

  getInitialState: ->
    if @props.params.houseId and HouseStore.get(@props.params.houseId)
      @getStateFromStores()

    else
      custom_question_count: 1
      form:
        title: null
        address: null
        city: null
        state: null
        price: null
        num_roommates: null
        num_bedrooms: null
        startDate: null
        endDate: null
        add_info: null
        online_payment_required: false
        references_required: false
        questions: []


  componentDidMount: ->
    HouseStore.addChangeListener(@houseChange)

    input = React.findDOMNode(@refs.address)
    options =
      types: [ 'address' ]

    addressAutocomplete = new (google.maps.places.Autocomplete)(input, options)
    google.maps.event.addListener addressAutocomplete, 'place_changed', =>
      @setAutocompleteAddress()
    @setState
      addressAutocomplete: addressAutocomplete

  componentWillUnmount: ->
    HouseStore.removeChangeListener(@houseChange)

  getStateFromStores: ->
    house = HouseStore.get(@props.params.houseId)

    if house
      house: house
      form:
        title: house.get('title')
        address: house.get('address')
        city: house.get('city')
        state: house.get('state')
        price: house.get('price')
        num_roommates: house.get('num_roommates')
        num_bedrooms: house.get('num_bedrooms')
        dateAvailable: house.get('dateAvailable')
        endDate: house.get('dateEnds')
        add_info: house.get('add_info')
        # images: house.get('images')
        online_payment_required: house.get('online_payment_required')
        references_required: house.get('references_required')

    else
      {}


  houseChange: ->
    @setState @getStateFromStores()

  addCustomQuestion: ->
    @setState
      custom_question_count: @state.custom_question_count+1

  removeCustomQuestion: ->
    count = @state.custom_question_count
    if count > 1
      count--
    @setState
      custom_question_count: count

  questionChanged: (index, event) ->
    value = event.target.value
    form = @state.form
    form.questions[index] = value

    @setState
      form: form

  setAutocompleteAddress: ->
    place = @state.addressAutocomplete.getPlace()
    comps = place['address_components']
    streetNumber = @extractPlaceComponent comps, 'street_number'
    street = @extractPlaceComponent comps, 'route'
    city = @extractPlaceComponent comps, 'locality'
    state = @extractPlaceComponent comps, 'administrative_area_level_1'

    form = @state.form
    form.address = streetNumber + ' ' + street
    form.city = city
    form.state = state

    @setState
      form: form

  extractPlaceComponent: (place, key) ->
    matched = _.find place, (comp) ->
      _.contains comp['types'], key
    if matched
      matched.long_name

  submit: ->
    if not UserStore.getCurrentUser()
      window.alert('must be logged in')
      return

    form = @state.form
    customQuestions = _.filter form.questions, (value, key) =>
      if not value or value == ''
        return false
      if key >= @state.custom_question_count
        return false
      return true

    form.questions = customQuestions

    if not @state.house
      ClientActions.submitNewListing(form)
    else
      ClientActions.submitEditListing(form, @state.house)

  dateChanged: (range) ->
    form = @state.form
    form.dateAvailable = range.start
    @setState
      form: form

  valueChanged: (event) ->
    form = @state.form
    {name,value} = event.target
    if name is 'price'
      value = parseInt(value, 10) or undefined
    if name == 'references_required'
      value = not form.references_required
    if name == 'online_payment_required'
      value = not form.online_payment_required
    form[name] = value
    @setState
      form: form

  _onValuesChanged: (key, values) ->
    form = @state.form
    form[key] = values
    @setState
      form: form
