Link = require('react-router').Link
SearchForm = require './search_form/search_form'
Map = require './map/map'
Constants = require('app/constants/constants')
Actions = Constants.ActionTypes
ServerActions = require('app/actions/server_actions')
HouseListings = require('app/components/house_listings/house_listings')
HouseStore = require 'app/stores/house_store'
SearchStore = require 'app/stores/search_store'

getStateFromStores = ->
  houses: HouseStore.getPage(SearchStore.getCurrentPage())

module.exports = React.createClass
  displayName: 'Search'

  render: ->
    <div>
      <div>
        <SearchForm />
        <Map houses={@state.houses} />
      </div>
      <div>
        <HouseListings houses={@state.houses} />
      </div>
    </div>

  componentWillMount: ->
    ServerActions.searchHouses()

  componentDidMount: ->
    HouseStore.addChangeListener(@_onChange)
    SearchStore.addChangeListener(@_onChange)

  componentWillUnmount: ->
    HouseStore.removeChangeListener(@_onChange)
    SearchStore.removeChangeListener(@_onChange)

  getInitialState: ->
    getStateFromStores()

  _onChange: ->
    @setState getStateFromStores()
