require './my_listing.scss'

Link = require('react-router').Link
Router = require 'react-router'
HouseStore = require 'app/stores/house_store'
ClientActions = require 'app/actions/client_actions'
FavoriteButton = require 'app/components/util/fav_button/fav_button'

module.exports = React.createClass
  displayName: 'MyListing'
  mixins: [Router.Navigation]

  render: ->

    <div className="my_listing" onClick={@onClick.bind(@, @props.house.id)} >
      <img src={@state.house.getMainImageUrl()} alt="house" width="200" height="100" />
      <div>
        {@props.house.get('title')}
      </div>
      <div>
        {@props.house.get('Address')}, {@props.house.get('City')}
      </div>
      <div>
        Status: {@props.house.getStatusText()}
      </div>
      <Link to='edit_listing' params={{houseId:@props.house.id}}>Edit Listing</Link>
    </div>

  getInitialState: ->
      houseId: null

  componentDidMount: ->
    # HouseStore.addChangeListener(@_onChange)

  componentWillUnmount: ->
    # HouseStore.removeChangeListener(@_onChange)

  onClick: (houseId) ->



