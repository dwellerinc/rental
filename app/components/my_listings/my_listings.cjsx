require './my_listings.scss'

Link = require('react-router').Link
HouseStore = require 'app/stores/house_store'
_ = require 'underscore'
ClientActions = require 'app/actions/client_actions'
MyListing = require 'app/components/my_listings/my_listing'
Router = require 'react-router'

getStateFromStores = ->
  {
    houses: HouseStore.getMyHouses()
  }

module.exports = React.createClass
  displayName: 'MyListings'
  mixins: [Router.Navigation]

  render: ->
    <div>
      <button onClick={@createNewListing}>Create New Listing</button> <br /> <br />
      {
        @state.houses.map (result) ->
          return <div className='my-listing'><MyListing house={result} key={result.id} /><br/></div>
      }
    </div>

  getInitialState: ->
    getStateFromStores()

  componentWillMount: ->
    ClientActions.fetchMyListings()

  componentDidMount: ->
    HouseStore.addChangeListener(@_onChange)

  componentWillUnmount: ->
    HouseStore.removeChangeListener(@_onChange)

  _onChange: ->
    @setState getStateFromStores()

  createNewListing: ->
    @transitionTo('/post')




