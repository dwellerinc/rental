Dispatcher = require('app/dispatcher')
Constants = require('app/constants/constants')
assign = require('object-assign')

EventEmitter = require('events').EventEmitter
ActionTypes = Constants.ActionTypes

CHANGE_EVENT = 'change'

currentUser = null

ThreadStore = assign({}, EventEmitter.prototype,
  init: (rawMessages) ->
    return
  emitChange: ->
    @emit CHANGE_EVENT
    return
  addChangeListener: (callback) ->
    @on CHANGE_EVENT, callback
    return
  removeChangeListener: (callback) ->
    @removeListener CHANGE_EVENT, callback
    return
  get: (id) ->
    return
  getCurrentUser: ->
    currentUser
)

ThreadStore.dispatchToken = Dispatcher.register((action) ->
  switch action.type
    when ActionTypes.APP_WILL_LAUNCH
      # currentUser = action.currentUser
      ThreadStore.emitChange()
    when ActionTypes.USER_REGISTERED
      currentUser = action.user
      ThreadStore.emitChange()
    when ActionTypes.LOGGED_IN
      currentUser = action.user
      ThreadStore.emitChange()
    when ActionTypes.LOGGED_OUT
      currentUser = null
      ThreadStore.emitChange()
    when ActionTypes.CURRENT_USER_UPDATED
      currentUser = action.user
      ThreadStore.emitChange()
  return
)

module.exports = ThreadStore