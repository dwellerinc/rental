Dispatcher = require('app/dispatcher')
Constants = require('app/constants/constants')

EventEmitter = require('events').EventEmitter
assign = require('object-assign')
ActionTypes = Constants.ActionTypes
_ = require 'underscore'

CHANGE_EVENT = 'house_store_change'

PAGE_SIZE = 10

current_house = null
searchHouses = []
myHouses = []
currentHouseId = null
allHouses = []
favorites = []

HouseStore = assign({}, EventEmitter.prototype,
  init: (rawMessages) ->
    return
  emitChange: ->
    @emit CHANGE_EVENT
    return
  addChangeListener: (callback) ->
    @on CHANGE_EVENT, callback
    return
  removeChangeListener: (callback) ->
    @removeListener CHANGE_EVENT, callback
    return
  get: (id) ->
    _.find allHouses, (house) ->
      house.id == id
  getAll: ->
    searchHouses
  getNumPages: ->
    Math.ceil(searchHouses.length / PAGE_SIZE)
  getPage: (page) ->
    startIndex = page * PAGE_SIZE
    endIndex = (page+1) * PAGE_SIZE
    searchHouses.slice(startIndex, endIndex)
  getPageSize: ->
    PAGE_SIZE
  getAllChrono: ->
    return
  getCurrentID: ->
    return
  getCurrent: ->
    # @get @getCurrentID()
  getCurrentHouseId: ->
    currentHouseId

  getMyHouses: ->
    myHouses

  addHouse: (house) ->
    searchHouses.push(house)
    existing = _.find allHouses, (h) -> house.id == h.id

    if not existing
      allHouses.push house

  isFavorite: (house) ->
    _.find favorites, (h) -> house.id == h.id

  getFavorites: ->
    favorites
)

addFavorite = (house) ->
  if not HouseStore.isFavorite(house)
    favorites.push(house)
  HouseStore.addHouse(house)

removeFavorite = (house) ->
  if HouseStore.isFavorite(house)
    favorites = favorites.filter (h) -> h.id != house.id

HouseStore.dispatchToken = Dispatcher.register((action) ->
  switch action.type
    when ActionTypes.RECEIVE_SEARCH_HOUSES
      searchHouses = action.houses
      HouseStore.emitChange()
    when ActionTypes.NEW_HOUSE_SUBMITTED
      house = action.house
      myHouses.push house
      HouseStore.addHouse(house)
      HouseStore.emitChange()
    when ActionTypes.RECEIVE_MY_LISTINGS
      houses = action.houses
      myHouses = houses
      _.each houses, (h) ->
        HouseStore.addHouse(h)
      HouseStore.emitChange()
    when ActionTypes.GO_TO_HOUSE_DETAILS
      currentHouseId = action.houseId
      HouseStore.emitChange()
    when ActionTypes.RECEIVE_HOUSE
      HouseStore.addHouse(action.house)
      HouseStore.emitChange()
    when ActionTypes.RECEIVE_HOUSES
      houses = action.houses
      _.each houses, (house) ->
        HouseStore.addHouse(house)
      HouseStore.emitChange()
    when ActionTypes.FAVORITE_CHANGED
      house = action.house
      favorite = action.favorite
      if favorite
        addFavorite(house)
      else
        removeFavorite(house)
      HouseStore.emitChange()
    when ActionTypes.FAVORITES_UPDATED
      houses = action.houses
      _.each houses, (house) ->
        addFavorite(house)
      HouseStore.emitChange()

  return
)

module.exports = HouseStore
