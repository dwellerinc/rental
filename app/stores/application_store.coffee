Dispatcher = require('app/dispatcher')
Constants = require('app/constants/constants')
assign = require('object-assign')
EventEmitter = require('events').EventEmitter
ActionTypes = Constants.ActionTypes
_  = require 'underscore'
UserStore = require 'app/stores/user_store'

CHANGE_EVENT = 'applicationStoreChange'

incomingApplications = []
outgoingApplications = []

AppStore = assign({}, EventEmitter.prototype,
  init: (rawMessages) ->
    return
  emitChange: ->
    @emit CHANGE_EVENT
    return
  addChangeListener: (callback) ->
    @on CHANGE_EVENT, callback
    return
  removeChangeListener: (callback) ->
    @removeListener CHANGE_EVENT, callback
    return
  get: (id) ->
    return
  getIncomingApplications: ->
    incomingApplications

  getIncomingApplicationsByHouseId: ->
    _.groupBy incomingApplications, (app) ->
      app.get('house').id

  getOutgoingApplications: ->
    outgoingApplications

  getOutgoingApplicationForHouse: (house) ->
    _.find outgoingApplications, (app) ->
      app.get('house').id == house.id

  addApplications: (applications) ->
    currentUser = UserStore.getCurrentUser()
    newOutgoing = _.filter applications, (app) ->
      if not app.get('user')
        debugger
      if not currentUser
        debugger
      app.get('user').id == currentUser.id
    outgoingApplications = outgoingApplications.concat newOutgoing

    newIncoming = _.filter applications, (app) ->
      app.get('user').id != currentUser.id
    incomingApplications = incomingApplications.concat newIncoming
)

AppStore.dispatchToken = Dispatcher.register((action) ->
  Dispatcher.waitFor([
    UserStore.dispatchToken
  ])

  switch action.type
    when ActionTypes.APPLICATION_SUBMITTED
      outgoingApplications.push action.application
      AppStore.emitChange()
    when ActionTypes.RECEIVE_APPLICATIONS
      AppStore.addApplications(action.applications)
      AppStore.emitChange()
  return
)

module.exports = AppStore