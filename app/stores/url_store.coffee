Dispatcher = require('app/dispatcher')
Constants = require('app/constants/constants')
assign = require('object-assign')
EventEmitter = require('events').EventEmitter
ActionTypes = Constants.ActionTypes

CHANGE_EVENT = 'urlChange'

url = null

ThreadStore = assign({}, EventEmitter.prototype,
  getNewUrl: ->
    url
  # init: (rawMessages) ->
  #   return
  # emitChange: ->
  #   @emit CHANGE_EVENT
  #   return
  # addChangeListener: (callback) ->
  #   @on CHANGE_EVENT, callback
  #   return
  # removeChangeListener: (callback) ->
  #   @removeListener CHANGE_EVENT, callback
  #   return
  # get: (id) ->
  #   return
  # getAll: ->
  #   houses
)

ThreadStore.dispatchToken = Dispatcher.register((action) ->
  switch action.type
    when ActionTypes.NAVIGATION
      url = action.url
      ThreadStore.emitChange()
  return
)

module.exports = ThreadStore