Dispatcher = require('app/dispatcher')
Constants = require('app/constants/constants')
_ = require('underscore')
snake = require('to-snake-case')
assign = require('object-assign')

EventEmitter = require('events').EventEmitter
ActionTypes = Constants.ActionTypes

CHANGE_EVENT = 'search_store_change'

searchParams = {}
currentPage = 0

ThreadStore = assign({}, EventEmitter.prototype,
  init: (rawMessages) ->
    return
  emitChange: ->
    @emit CHANGE_EVENT
    return
  addChangeListener: (callback) ->
    @on CHANGE_EVENT, callback
    return
  removeChangeListener: (callback) ->
    @removeListener CHANGE_EVENT, callback
    return
  get: (id) ->
    return
  getAll: ->
    houses
  setParams: (key, values) ->
    searchParams[key] = values
  getCurrentPage: ->
    currentPage

)

ThreadStore.dispatchToken = Dispatcher.register((action) ->
  switch action.type
    when ActionTypes.SEARCH_PARAMS_CHANGED
      params = action.params
      currentPage = 0
      ThreadStore.emitChange()
    when ActionTypes.SEARCH_PAGE_CHANGED
      currentPage = action.page
      ThreadStore.emitChange()
  return
)

module.exports = ThreadStore