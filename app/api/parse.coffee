Constants = require('app/constants/constants')
Attributes = require('app/constants/attributes')
ServerActions = require('app/actions/server_actions')
superagent = require('superagent')
snake = require('to-snake-case')
_ = require('underscore')
User = require 'app/models/user'
House = require 'app/models/house'
Application = require 'app/models/application'

module.exports =

  House: null
  Application: null

  init: ->
    if @getCurrentUser()
      query = new Parse.Query(User)
      query.include('favorites')
      query.equalTo('objectId', @getCurrentUser().id)
      query.find().then((users) ->
        ServerActions.currentUserUpdated(users[0])
        ServerActions.favoritesUpdated(users[0].get('favorites'))
      ).then(=>
        @fetchApplications()
        @fetchMyListings()

      )
    @searchHouses()

  getCurrentUser: ->
    user = Parse.User.current()
    user

  registerUser: (params) ->
    user = new Parse.User()

    _.each params, (val, key) ->
      user.set(key, val)

    user.signUp null, {
      success: (user) ->
        ServerActions.userRegistered(user)
      error: (user, error) ->
        console.log(error)

    }

  logout: ->
    Parse.User.logOut()
    ServerActions.loggedOut()

  login: (username, password) ->
    Parse.User.logIn username, password, {
      success: (user) ->
        ServerActions.loggedIn(user)
      error: (user, error) ->
        console.log(error)
    }

  searchHouses: (params)->
    query = new Parse.Query(House)
    query.limit(1000)
    currentUser = @getCurrentUser()
    if currentUser
      query.notEqualTo('user', currentUser)
    if params
      _.each params, (val, key) ->
        if not val
          return

        if val.length > 0
          query.containsAll(key, val)
        else if 'start' of val and 'end' of val
          if val.end?
            query.lessThanOrEqualTo(key, val.end)
          if val.start?
            query.greaterThanOrEqualTo(key, val.start)
        else if not val.length?
          query.equalTo(key, val)

    return query.find().then (houses) ->
      ServerActions.receiveSearchHouses(houses)

  fetchHouse: (houseId) ->
    query = new Parse.Query(House)
    query.get(houseId).then (house) ->
      ServerActions.receiveHouse house
    , (error) ->
      console.log(error)

  toggleFavorite: (house, favorite) ->
    user = @getCurrentUser()

    if favorite
      user.addUnique('favorites', house)
    else
      user.remove('favorites', house)

    user.save().then (user) ->
      ServerActions.favoriteChanged(house, favorite)

  submitListing: (form, house) ->

    house ?= new House()

    if not form.images?.length
      console.log 'you need some images!'
      return

    parseImages = form.images.map (image, i) ->
      new Parse.File "listing-#{i}", {base64: image}

    Parse.Promise.when(
      parseImages.map (image) -> image.save()
    ).then( (a) =>
      parseFiles = Array.prototype.slice.call arguments
      house.unset 'images'
      _.each parseFiles, (file) ->
        data = { __type:'File', name: file.name, url: file.url() }
        house.add 'images', data

      _.each form, (val, key) =>
        return if key is 'images'
        if key and val
          if key == 'questions'
            _.each val, (question, key) ->
              house.add('questions', {id: key, text: question})
          else if Array.isArray val
            _.each val, (attr) =>
              house.addUnique key, attr
          else
            house.set key, val

      house.set 'Address', form.address
      house.set 'user', @getCurrentUser()

      address = form.address
      geocodeURL = "https://maps.googleapis.com/maps/api/geocode/json?address=#{address}&bounds=37.682727,-122.561405|37.827678,-122.353351&key=AIzaSyAIOJXM3vKv4dyiVtHVQSCtGwPasDhII5E"
      superagent.get(geocodeURL).end (err, response) =>
        response = JSON.parse response.text
        results = response.results
        sfPlace = _.find results, (result) ->
          result.formatted_address.indexOf "San Francisco"

        geo = sfPlace?.geometry?.location
        if not geo
          console.log 'location not found'
          return

        lat = geo.lat
        lng = geo.lng
        house.set('lat', lat)
        house.set('lng', lng)

        house.save().then (house) -> ServerActions.newHouseSubmitted house

    ).then null, (error) -> console.log error

  submitApplication: (house, form) ->
    user = @getCurrentUser()
    app = new Application
    app.set('house', house)

    _.each form, (val, key) =>
      if key and val
        return if key == 'references'
        if key == 'questions'
          _.each val, (question, key) ->
            app.add('questions', {id: key, text: question})
        else if Array.isArray val
          _.each val, (attr) =>
            app.addUnique key, attr
        else
          app.set key, val

    app.set('user', user)

    app.save().then (application) =>
      ServerActions.applicationSubmitted(application)
      if form.references
        @updateCurrentUser({references: form.references})
    , (error) ->
      console.log(error)

  fetchApplications: ->
    userQuery = new Parse.Query(Application)
    userQuery.equalTo('user', @getCurrentUser())
    userQuery.include('house')

    houseQuery = new Parse.Query(House)
    houseQuery.equalTo('user', @getCurrentUser())
    housesQuery = new Parse.Query(Application)
    housesQuery.matchesQuery('house', houseQuery)
    housesQuery.include('house')

    Parse.Query.or(userQuery, housesQuery).find().then (applications) =>
      ServerActions.receiveApplications(applications)
      houseIds = _.map applications, (app) ->
        app.get('house').id

      # fetch all the houses from the applications
      houseQuery = new Parse.Query(House)
      houseQuery.containedIn('objectId', houseIds)
      houseQuery.find().then (houses) ->
        ServerActions.receiveHouses houses

    , (error) ->
      console.log error

  fetchMyListings: ->
    user = @getCurrentUser()
    query = new Parse.Query(House)
    query.equalTo('user', user)
    query.find().then (houses) ->
      ServerActions.receiveMyListings(houses)
    , (error) ->
      console.log(error)

  updateCurrentUser: (form) ->
    user = @getCurrentUser()

    _.each form, (val, key) =>
      if Array.isArray val
        _.each val, (v) ->
          user.add(key, v)
      else
        user.set(key, val)

    user.save().then (user) ->
      ServerActions.currentUserUpdated(user)
    , (error) ->
      console.log error

  facebookLogin: ->
    Parse.FacebookUtils.logIn('email,user_birthday',
      success: (user) ->
        FB.api '/me?fields=first_name,last_name,email,birthday,id,link,picture', (response) ->
          user.set('firstName', response.first_name)
          user.set 'lastName', response.last_name
          user.set 'email', response.email
          user.set 'username', response.email
          birthday = new Date(response.birthday)
          user.set 'birthday', birthday
          user.set 'facebookId', response.id
          user.set 'facebookUrl', response.link
          user.set 'fbPicture', response.picture.data.url
          user.save().then (user) ->
            ServerActions.loggedIn(user)
          , (error) ->
            console.log error

        if not user.existed()
          console.log("User signed up and logged in through Facebook!")
        else
          console.log("User logged in through Facebook!")
      error: (user, error) ->
        console.log user
        console.log error
        console.log("User cancelled the Facebook login or did not fully authorize.")
    )

  linkFacebookAccount: ->
    Parse.FacebookUtils.link(@getCurrentUser(), null,
      success: (user) ->
        FB.api '/me', (response) ->
          user.set 'facebookId', response.id
          user.set 'facebookUrl', response.link
          user.save().then (user) ->
            ServerActions.facebookLinked()
        console.log("Woohoo, user logged in with Facebook!")
      error: (user, error) ->
        console.log error
        console.log("User cancelled the Facebook login or did not fully authorize.")
    )

  geocodeHouses: ->
    query = new Parse.Query(House).doesNotExist('lat')
    return query.find().then (houses) ->
      delay = 0
      _.each houses, (house) =>
        setTimeout ->
          address = encodeURIComponent(house.get('Address'))
          url = "https://maps.googleapis.com/maps/api/geocode/json?address=#{address}&bounds=37.682727,-122.561405|37.827678,-122.353351&key=AIzaSyAIOJXM3vKv4dyiVtHVQSCtGwPasDhII5E"
          console.log(url)
          superagent.get(url).end (err, response) =>
            console.log(response)
            response = JSON.parse response.text
            results = response.results
            sfPlace = _.find results, (result) ->
              result.formatted_address.indexOf "San Francisco"

            geo = sfPlace.geometry.location
            lat = geo.lat
            lng = geo.lng
            console.log(geo)
            house.set('lat', lat)
            house.set('lng', lng)
            house.save()
        , delay

        delay += 100

  addRandomHouseAttributes: ->
    ammenities = Attributes.Ammenities
    ammenities = _.map ammenities, (amm) ->
      snake amm
    query = new Parse.Query(House)
    query.limit(1000)
    query.doesNotExist('ammenities')
    query.find().then (houses) ->
      delay = 0
      setTimeout ->
      _.each houses, (house) ->
        houseAmmenities = _.filter ammenities, ->
          Math.random() > 0.5
        _.each houseAmmenities, (hAmm) ->
          house.addUnique('ammenities', hAmm)

        house.save()

      , delay

      delay += 50

