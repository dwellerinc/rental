Dispatcher = require('app/dispatcher')
Constants = require('app/constants/constants')
parse = require('app/api/parse')

ActionTypes = Constants.ActionTypes

module.exports =
  searchParamsChanged: (params) ->
    Dispatcher.dispatch
      type: ActionTypes.SEARCH_PARAMS_CHANGED
      params: params

    parse.searchHouses(params)

  searchPageChanged: (page) ->
    Dispatcher.dispatch
      type: ActionTypes.SEARCH_PAGE_CHANGED
      page: page

  appWillLaunch: ->
    Dispatcher.dispatch
      type: ActionTypes.APP_WILL_LAUNCH
      currentUser: parse.getCurrentUser()

  registerUser: (form) ->
    form.username = form.email
    console.log form.username
    console.log form.email

    Dispatcher.dispatch
      type: ActionTypes.REGISTER_USER
      form: form

    parse.registerUser(form)

  logout: ->
    parse.logout()

    Dispatcher.dispatch
      type: ActionTypes.LOGOUT

  login: (username, password) ->
    Dispatcher.dispatch
      type: ActionTypes.LOGGING_IN

    parse.login(username, password)

  facebookLogin: ->
    Dispatcher.dispatch
      type: ActionTypes.FACEBOOK_LOGIN

    parse.facebookLogin()

  linkFacebookAccount: ->
    Dispatcher.dispatch
      type: ActionTypes.LINK_FACEBOOK_ACCOUNT

    parse.linkFacebookAccount()

  submitNewListing: (form) ->
    Dispatcher.dispatch
      type: ActionTypes.SUBMIT_NEW_LISTING
      form: form

    parse.submitListing(form)

  submitEditListing: (form, house) ->
    Dispatcher.dispatch
      type: ActionTypes.SUBMIT_EDIT_LISTING
      form: form
      house: house

    parse.submitListing(form, house)

  fetchMyListings: ->
    Dispatcher.dispatch
      type: ActionTypes.FETCH_MY_LISTINGS

    parse.fetchMyListings()

  goToHouseDetails: (houseId) ->
    Dispatcher.dispatch
      type: ActionTypes.GO_TO_HOUSE_DETAILS
      houseId: houseId

  navigateToUrl: (url) ->
    Dispatcher.dispatch
      type: ActionTypes.NAVIGATION
      url: url

  fetchHouse: (houseId) ->
    Dispatcher.dispatch
      type: ActionTypes.FETCH_HOUSE
      houseId: houseId

    parse.fetchHouse(houseId)

  updateCurrentUser: (form) ->
    Dispatcher.dispatch
      type: ActionTypes.UPDATE_CURRENT_USER
      form: form

    parse.updateCurrentUser(form)

  submitApplication: (house, form) ->
    Dispatcher.dispatch
      type: ActionTypes.SUBMIT_APPLICATION
      house: house
      form: form

    parse.submitApplication(house, form)

  toggleFavorite: (house, favorite) ->
    Dispatcher.dispatch
      type: ActionTypes.TOGGLE_FAVORITE
      house: house
      favorite: favorite

    parse.toggleFavorite(house, favorite)

