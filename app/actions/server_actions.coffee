Dispatcher = require('app/dispatcher')
Constants = require('app/constants/constants')

ActionTypes = Constants.ActionTypes

module.exports =
  searchHouses: (params) ->
    Dispatcher.dispatch
      type: ActionTypes.SEARCH_HOUSES
      params: params

  receiveSearchHouses: (houses) ->
    Dispatcher.dispatch
      type: ActionTypes.RECEIVE_SEARCH_HOUSES
      houses: houses

  receiveHouses: (houses) ->
    Dispatcher.dispatch
      type: ActionTypes.RECEIVE_HOUSES
      houses: houses

  userRegistered: (user) ->
    Dispatcher.dispatch
      type: ActionTypes.USER_REGISTERED
      user: user

  loggedIn: (user) ->
    Dispatcher.dispatch
      type: ActionTypes.LOGGED_IN
      user: user

  loggedOut: ->
    Dispatcher.dispatch
      type: ActionTypes.LOGGED_OUT

  facebookLinked: ->
    Dispatcher.dispatch
      type: ActionTypes.FACEBOOK_LINKED

  newHouseSubmitted: (house) ->
    Dispatcher.dispatch
      type: ActionTypes.NEW_HOUSE_SUBMITTED
      house: house

  receiveMyListings: (houses) ->
    Dispatcher.dispatch
      type: ActionTypes.RECEIVE_MY_LISTINGS
      houses: houses

  receiveHouse: (house) ->
    Dispatcher.dispatch
      type: ActionTypes.RECEIVE_HOUSE
      house: house

  currentUserUpdated: (user) ->
    Dispatcher.dispatch
      type: ActionTypes.CURRENT_USER_UPDATED
      user: user

  applicationSubmitted: (app) ->
    Dispatcher.dispatch
      type: ActionTypes.APPLICATION_SUBMITTED
      application: app

  receiveApplications: (apps) ->
    Dispatcher.dispatch
      type: ActionTypes.RECEIVE_APPLICATIONS
      applications: apps

  favoriteChanged: (house, favorite) ->
    Dispatcher.dispatch
      type: ActionTypes.FAVORITE_CHANGED
      house: house,
      favorite: favorite

  favoritesUpdated: (favorites) ->
    Dispatcher.dispatch
      type: ActionTypes.FAVORITES_UPDATED
      houses: favorites
