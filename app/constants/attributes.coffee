module.exports =
  'lifestyle':
    pretty: 'Lifestyle'
    values: [
      {
        key:'traveller'
        value: 'Traveller'
      },
      {
        key:'works_alot'
        value: 'Works alot'
      },
      {
       key:'home_alot'
       value:'Home alot'
      },
      {
        key:'partying'
        value: 'Partying Scene'
      },
      {
        key:'cooking'
        value: 'Cooking'
      },
      {
        key:'loud_music'
        value: 'Loud Music'
      }
    ]

  'personality':
    pretty: 'Personality'
    values: [
      {
        key:'outgoing'
        value: 'Outgoing'
      },
      {
        key:'relaxed'
        value: 'Relaxed'
      },
      {
       key:'shy'
       value:'Shy'
      },
      {
        key:'quiet'
        value: 'Quiet'
      },
      {
        key:'hippy'
        value: 'Hippy'
      },
      {
        key:'artsy'
        value: 'Artsy'
      },
      {
        key:'techy'
        value: 'Techy'
      }
    ]

  'activities':
    pretty: 'Activities'
    values: [
      {
        key:'sports'
        value: 'Sports'
      },
      {
        key:'programming'
        value: 'Programming'
      },
      {
       key:'420'
       value:'420'
      },
      {
        key:'partying'
        value: 'Partying'
      },

    ]
  'nearby_services':
    pretty: 'Nearby Services'
    values: [
      {
        key:'fitness'
        value: 'Fitness'
      },
      {
        key:'restaurants'
        value: 'Restaurants'
      },
      {
       key:'parks'
       value:'Parks'
      },
      {
        key:'uber_hotspot'
        value: 'Uber Hotspot'
      },
      {
        key:'bank'
        value: 'Bank'
      },
      {
        key:'cafe'
        value: 'Cafe'
      },
      {
        key:'religious'
        value: 'Religious'
      },
      {
        key:'shopping'
        value: 'Shopping'
      },
      {
        key:'subway_stop'
        value: 'Bart'
      },
      {
        key:'bus_stop'
        value: 'Muni Stop'
      }
    ]

  'ammenities':
    pretty: 'Ammenities'
    values: [
      {
        key:'dishwasher'
        value: 'Dishwasher'
      },
      {
        key:'rooftop'
        value: 'Rooftop'
      },
      {
       key:'gym'
       value:'Gym'
      },
      {
        key:'laundry'
        value: 'Laundry'
      },
      {
        key:'air_conditioning'
        value: 'Air conditioning'
      },
      {
        key:'central_heat'
        value: 'Central Heat'
      },
      {
        key:'elevator'
        value: 'Elevator'
      },
      {
        key:'doorman'
        value: 'Doorman'
      },
      {
        key:'balcony'
        value: 'Balcony'
      },
      {
        key:'terrace'
        value: 'Terrace'
      }
    ]

  'type':
    pretty: 'Type'
    values: [
      {
        key:'house'
        value: 'House'
      },
      {
        key:'appartment'
        value: 'Appartment'
      },
      {
        key:'group'
        value: 'Group'
      },
      {
        key:'women_only'
        value: 'Women Only'
      }
    ]

  'living_space':
    pretty: 'Living Space'
    values: [
      {
        key:'private'
        value: 'Private'
      },
      {
        key:'shared'
        value: 'Shared'
      },
      {
        key:'other'
        value: 'Other'
      }
    ]
