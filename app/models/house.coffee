
module.exports = Parse.Object.extend('house',
  {
    getStatusText: ->
      if not @get('status')
        'Waiting for Applications'
      else @get('status')

    getMainImageUrl: ->
      images = @get('images') or []
      if images[0] and images[0].url
        image = images[0].url()
      else
        image = images[0] or 'images/house.jpg'

      image

  }, {
    #class methods

  }

)



