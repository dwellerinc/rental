# Load css first thing. It gets injected in the <head> in a <style> element by
# the Webpack style-loader.
require '../../public/main.css'

React = require 'react'
# Assign React to Window so the Chrome React Dev Tools will work.
window.React = React

Router = require('react-router')
Route = Router.Route

# Require route components.
HelloWorld = require './../components/hello_world'
StyleGuide = require './../components/styleguide'
Search = require './../components/search'
App = require './../components/app'
PostListing = require 'app/components/post_listing/post_listing'
Applications = require 'app/components/applications/applications'
MyListings = require 'app/components/my_listings/my_listings'
UserInfo = require 'app/components/user_info/user_info'
Login = require 'app/components/user_info/login'
Register = require 'app/components/user_info/register'
HouseDetails = require 'app/components/house_details/house_details'
ApplicationPage = require 'app/components/application_page/application_page'
Favorites = require 'app/components/favorites/favorites'
Messages = require 'app/components/messages/messages'


routes = (
  <Route handler={App}>
  	<Route name="search" handler={Search} path="/" />
    <Route name="hello" handler={HelloWorld} path="/hello" />
    <Route name="styleguide" handler={StyleGuide} path="/styleguide" />
    <Route name='post' handler={PostListing} path='/post' />
    <Route name='applications' handler={Applications} path='/applications' />
    <Route name='my_listings' handler={MyListings} path='/my_listings' />
    <Route name='user_info' handler={UserInfo} path='/user_info' />
    <Route name='login' handler={Login} path='/login' />
    <Route name='register' handler={Register} path='/register' />
    <Route name='house_details' handler={HouseDetails} path="/house_details/:houseId" />
    <Route name='application' handler={ApplicationPage} path="/application/:houseId" />
    <Route name='favorites' handler={Favorites} path='/favorites' />
    <Route name='messages' handler={Messages} path='/messages' />
    <Route name='edit_listing' handler={PostListing} path='/edit_listing/:houseId' />
  </Route>
)

anchor = document.getElementById 'react-anchor'

Router.run(routes, (Handler) ->
  React.render <Handler/>, anchor
)
